@echo off
echo.--------------------------------------
echo.Splash Logo Image Maker
echo.
echo.	By **Gokul NC**
echo.--------------------------------------
echo.
echo.
echo.Creating splash.img ........
echo.
echo.
echo.
if not exist "output\" mkdir "output\"
del /Q output\splash.img 2>NUL

set pic_path="not_found"
if exist "pics\logo.png" set pic_path="pics\logo.png"
if exist "pics\logo.jpg" set pic_path="pics\logo.jpg"
if exist "pics\logo.jpeg" set pic_path="pics\logo.jpeg"
if exist "pics\logo.bmp" set pic_path="pics\logo.bmp"
if exist "pics\logo.gif" set pic_path="pics\logo.gif"
if %pic_path%=="not_found" echo.logo.png or logo.jpg not found in 'pics' folder.. EXITING&echo.&echo.&pause&exit

bin\Python2.7\python2.7.exe bin\logo_gen.py %pic_path% || (echo."PROCESS FAILED. MAKE SURE IMAGE is PROPER PNG format"&echo.Quitting&echo.&pause&exit)

if exist "output\splash.img" ( echo.SUCCESS!&echo.splash.img created in "output" folder
) else ( echo.PROCESS FAILED.. Try Again&echo.&echo.&pause&exit )

echo.&echo.&set /P INPUT=Do you want to create a flashable zip? [yes/no]
If /I "%INPUT%"=="y" goto :CREATE_ZIP
If /I "%INPUT%"=="yes" goto :CREATE_ZIP

echo.&echo.&echo Flashable ZIP not created..&echo.&echo.&pause&exit

:CREATE_ZIP
copy /Y bin\New_Splash.zip output\flashable_splash.zip >NUL
cd output
..\bin\7za a flashable_splash.zip splash.img >NUL
cd..

if exist "output\flashable_splash.zip" (
 echo.&echo.&echo.SUCCESS!
 echo.Flashable zip file created in "output" folder
 echo.You can flash the flashable_splash.zip from any custom recovery like TWRP or CWM or Philz
) else ( echo.&echo.&echo Flashable ZIP not created.. )

echo.&echo.&pause&exit